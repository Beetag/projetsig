openlayers : 

- cd openlayers
- npm install
- pour run : npm start

android :

- créer un fichier "url.xml" dans res/values
- y insérer :
<resources>
    <string name="openlayers_url">ip_locale:port_openlayers</string>
    <string name="api_url">ip_locale:port_api_rest</string>
</resources>
- avec les valeurs correspondant à votre installation
- vérifiez que le fichier n'est pas suivit par git

geoserver :
- noms à respecter :
	- workspace : "sig"
	- layer group : "etage1", "etage2"

- créer un layer par classe (piece, personne, points d'accès)
et par étage (1 & 2) soit 6 layers
- mettre un style personnalisé à la personne pour la distinguer
- créer un layer group par étage contenant chaque classe
