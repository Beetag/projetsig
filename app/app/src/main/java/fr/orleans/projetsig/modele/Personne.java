package fr.orleans.projetsig.modele;


import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.locationtech.jts.geom.Geometry;

public class Personne {
    private long idpersonne;

    private int etage;

    private Geometry position;

    public long getIdpersonne() {
        return idpersonne;
    }

    public void setIdpersonne(long idpersonne) {
        this.idpersonne = idpersonne;
    }

    public int getEtage() {
        return etage;
    }

    public void setEtage(int etage) {
        this.etage = etage;
    }

    public Geometry getPosition() {
        return position;
    }

    public void setPosition(Geometry position) {
        this.position = position;
    }
}
