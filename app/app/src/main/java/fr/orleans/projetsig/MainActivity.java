package fr.orleans.projetsig;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bedatadriven.jackson.datatype.jts.JtsModule;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.locationtech.jts.geom.Coordinate;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import fr.orleans.projetsig.dto.CheminDTO;
import fr.orleans.projetsig.modele.Personne;
import fr.orleans.projetsig.modele.Piece;
import fr.orleans.projetsig.modele.PointAcces;
import fr.orleans.projetsig.modele.Voisin;

public class MainActivity extends AppCompatActivity {

    private static final int EDIT_ACTIVITY_RESULT_VALUE = 43;
    private WebView webView;
    private Spinner spinnerChemin;
    private Spinner spinnerDeplacement;
    private Personne pers = null;
    private List<Piece> pieces = null;
    private List<PointAcces> pointsAcces = null;
    private Coordinate arriveeChemin = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            this.getPersonne();
            this.getPieces();
            this.getPointsAcces();
        } catch (IOException e) {
            e.printStackTrace();
        }

        webView = (WebView) findViewById(R.id.webView);

        spinnerChemin = (Spinner) findViewById(R.id.listePointsChemin);
        spinnerDeplacement = (Spinner) findViewById(R.id.listePointsDeplacement);

        //Permet le rafraichissement d'openlayers à chaud
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            this.getPersonne();
            this.getPieces();
            this.getPointsAcces();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 43) {
            this.onResume();
        }
    }

    public void onClickChemin(View view) throws IOException, JSONException {
        //cas pos pers = pos choisie
        // toast
        PointAcces pa = (PointAcces) spinnerChemin.getSelectedItem();

        // On vérifie d'abord que le point choisi n'est pas sur place
        if (pa.getGeom().getCoordinate().compareTo(this.pers.getPosition().getCoordinate()) == 0) {
            Toast.makeText(MainActivity.this, R.string.same_pos, Toast.LENGTH_LONG).show();
        } else {
            CheminDTO chemin = new CheminDTO();
            chemin.setIdArrivee(pa.getIdpoint());
            postChemin(chemin);
            this.arriveeChemin = pa.getGeom().getCoordinate();
        }
    }

    public void onClickDeplacement(View view) {
        PointAcces pa = (PointAcces) spinnerDeplacement.getSelectedItem();
        try {
            deplacement(pa);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    public void onClickEditPiece(View view) {
        ArrayList<Piece> arl_pieces = new ArrayList<>(this.pieces);
        Intent intent = new Intent(this, EditPieceActivity.class);
        intent.putExtra("pieces", arl_pieces);
        startActivityForResult(intent, EDIT_ACTIVITY_RESULT_VALUE);
    }

    private void updateWebView() {
        String url = getResources().getString(R.string.openlayers_url)
                + "?etage=" + this.pers.getEtage();
        this.webView.loadUrl(url);
    }

    private void getPersonne() throws IOException {
        String url = getResources().getString(R.string.api_url) + "/personnes/1";
        URL url_personne = new URL(url);
        new GetPersonne().execute(url_personne);
    }

    private void getPieces() throws IOException {
        String url = getResources().getString(R.string.api_url) + "/pieces";
        URL url_pieces = new URL(url);
        new GetPieces().execute(url_pieces);
    }

    private void getPointsAcces() throws IOException {
        String url = getResources().getString(R.string.api_url) + "/point_acces";
        URL url_pieces = new URL(url);
        new GetPointsAcces().execute(url_pieces);
    }

    private void deplacement(PointAcces pointAcces) throws IOException, JSONException {

        // création de l'objet a envoyer en fonction des valeurs choisies
        Personne p = new Personne();
        p.setEtage(pointAcces.getEtage());
        p.setPosition(pointAcces.getGeom());

        // création url requête
        String url = getResources().getString(R.string.api_url)
                + "/personnes/" + this.pers.getIdpersonne();
        URL url_personne = new URL(url);

        // Conversion personne en Json
        ObjectMapper om = new ObjectMapper();
        om.registerModule(new JtsModule());
        JSONObject persJson = new JSONObject(om.writeValueAsString(p));
        //Envoi de la requete PUT
        new EditPersonne(url_personne, persJson).execute();
    }

    private void setSpinners() {
        ArrayAdapter<PointAcces> dataAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, this.pointsAcces);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerChemin.setAdapter(dataAdapter);
        spinnerDeplacement.setAdapter(dataAdapter);
    }

    private void postChemin(CheminDTO cheminDTO) throws IOException, JSONException {
        // création url requête
        String url = getResources().getString(R.string.api_url)
                + "/trouverchemin/";
        URL url_chemin = new URL(url);

        // Conversion personne en Json
        ObjectMapper om = new ObjectMapper();
        om.registerModule(new JtsModule());
        JSONObject cheminJson = new JSONObject(om.writeValueAsString(cheminDTO));
        //Envoi de la requete PUT
        new PostChemin(url_chemin, cheminJson).execute();
    }

    private void deleteChemin() throws MalformedURLException {
        String url = getResources().getString(R.string.api_url) + "/chemins";
        URL url_delete_chemin = new URL(url);
        new DeleteChemin().execute(url_delete_chemin);
    }

    private class GetPersonne extends AsyncTask<URL, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... urls) {
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) urls[0].openConnection();
                int response = connection.getResponseCode();
                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder builder = new StringBuilder();

                    try (BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream()))) {
                        String line;

                        while ((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                    }
                    catch (IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, R.string.read_error, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    return new JSONObject(builder.toString());
                }
                else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            }

            finally {
                connection.disconnect(); // fermeture de la connexion
            }

            return null;
        }

        // Traitement de la réponse
        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            System.out.println(jsonObject);
            ObjectMapper om = new ObjectMapper();
            om.registerModule(new JtsModule());
            try {
                Personne p = om.readValue(jsonObject.toString(), Personne.class);
                updatePersonne(p);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class GetPieces extends AsyncTask<URL, Void, JSONArray> {

        @Override
        protected JSONArray doInBackground(URL... urls) {
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) urls[0].openConnection();
                int response = connection.getResponseCode();
                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder builder = new StringBuilder();

                    try (BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream()))) {
                        String line;

                        while ((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                    }
                    catch (IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, R.string.read_error, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    return new JSONArray(builder.toString());
                }
                else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            }

            finally {
                connection.disconnect(); // fermeture de la connexion
            }

            return null;
        }

        // Traitement de la réponse
        @Override
        protected void onPostExecute(JSONArray jsonArray) {
//            System.out.println(jsonArray);
            ObjectMapper om = new ObjectMapper();
            om.registerModule(new JtsModule());
            try {
                List<Piece> pieces = Arrays.asList(om.readValue(jsonArray.toString(), Piece[].class));
                System.out.println(pieces);
                updatePieces(pieces);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class GetPointsAcces extends AsyncTask<URL, Void, JSONArray> {

        @Override
        protected JSONArray doInBackground(URL... urls) {
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) urls[0].openConnection();
                int response = connection.getResponseCode();
                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder builder = new StringBuilder();

                    try (BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream()))) {
                        String line;

                        while ((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                    }
                    catch (IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, R.string.read_error, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    return new JSONArray(builder.toString());
                }
                else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            }

            finally {
                connection.disconnect(); // fermeture de la connexion
            }

            return null;
        }

        // Traitement de la réponse
        @Override
        protected void onPostExecute(JSONArray jsonArray) {
//            System.out.println(jsonArray);
            ObjectMapper om = new ObjectMapper();
            om.registerModule(new JtsModule());
            try {
                List<PointAcces> points = Arrays.asList(om.readValue(jsonArray.toString(), PointAcces[].class));
                System.out.println(points);
                updatePointsAcces(points);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class EditPersonne extends AsyncTask<Void, Void, JSONObject> {
        private URL url;
        private JSONObject jsonObject;

        public EditPersonne(URL url, JSONObject jsonObject) {
            this.url = url;
            this.jsonObject = jsonObject;
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) this.url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "*/*");
                connection.setRequestMethod("PUT");

                DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                out.writeBytes(this.jsonObject.toString());
                out.flush();
                out.close();

                int response = connection.getResponseCode();
                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder builder = new StringBuilder();

                    try (BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream()))) {
                        String line;

                        while ((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                    }
                    catch (IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, R.string.read_error, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    return new JSONObject(builder.toString());
                }
                else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            }

            finally {
                connection.disconnect(); // fermeture de la connexion
            }

            return null;
        }

        // Traitement de la réponse
        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            System.out.println(jsonObject);
            ObjectMapper om = new ObjectMapper();
            om.registerModule(new JtsModule());
            try {
                Personne p = om.readValue(jsonObject.toString(), Personne.class);
                System.out.println(p);
                updatePersonne(p);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class PostChemin extends AsyncTask<Void, Void, JSONArray> {
        private URL url;
        private JSONObject jsonObject;

        public PostChemin(URL url, JSONObject jsonObject) {
            this.url = url;
            this.jsonObject = jsonObject;
        }

        @Override
        protected JSONArray doInBackground(Void... params) {
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) this.url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "*/*");
                connection.setRequestMethod("POST");

                DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                out.writeBytes(this.jsonObject.toString());
                out.flush();
                out.close();

                int response = connection.getResponseCode();
                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder builder = new StringBuilder();

                    try (BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream()))) {
                        String line;

                        while ((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                    }
                    catch (IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, R.string.read_error, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    return new JSONArray(builder.toString());
                }
                else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            }

            finally {
                connection.disconnect(); // fermeture de la connexion
            }

            return null;
        }

        // Traitement de la réponse
        @Override
        protected void onPostExecute(JSONArray jsonArray) {
            System.out.println(jsonArray);
            // Pas nécessaire de traiter la réponse
            updateWebView();
        }
    }

    private class DeleteChemin extends AsyncTask<URL, Void, Void> {

        @Override
        protected Void doInBackground(URL... urls) {
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) urls[0].openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "*/*");
                connection.setRequestMethod("DELETE");

                int response = connection.getResponseCode();
                if (response != HttpURLConnection.HTTP_OK) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            }

            finally {
                connection.disconnect(); // fermeture de la connexion
            }

            return null;
        }

        // Traitement de la réponse
        @Override
        protected void onPostExecute(Void v) {
            updateWebView();
        }
    }

    private void updatePersonne(Personne p) throws MalformedURLException {
        this.pers = p;
        if (this.arriveeChemin != null) {
            if (this.pers.getPosition().getCoordinate().compareTo(
                    this.arriveeChemin) == 0) {
                this.deleteChemin();
                this.arriveeChemin = null;
            }
        }
        this.updateWebView();
    }

    private void updatePieces(List<Piece> pieces) {
        this.pieces = pieces;
    }

    private void updatePointsAcces(List<PointAcces> points) {
        this.pointsAcces = points;
        this.setSpinners();
    }
}