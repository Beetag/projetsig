package fr.orleans.projetsig;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bedatadriven.jackson.datatype.jts.JtsModule;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.orleans.projetsig.modele.Piece;

public class EditPieceActivity extends AppCompatActivity {
    private static final int ACTIVITY_RESULT_VALUE = 43;
    private Spinner piecesSpinner;
    private Spinner typesSpinner;
    private EditText nom_salle_ET;
    private List<Piece> pieces;
    private final List<String> types_salle = Arrays.asList("td", "tp", "amphi", "admin");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_piece);

        piecesSpinner = (Spinner) findViewById(R.id.pieces_spinner);
        typesSpinner = (Spinner) findViewById(R.id.type_salle_spinner);
        nom_salle_ET = (EditText) findViewById(R.id.nom_salle_ET);

        this.pieces = (ArrayList<Piece>) getIntent().getSerializableExtra("pieces");

        ArrayAdapter<Piece> dataAdapterPiece = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, this.pieces);
        dataAdapterPiece.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        piecesSpinner.setAdapter(dataAdapterPiece);

        ArrayAdapter<String> dataAdapterTypes = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, this.types_salle);
        dataAdapterTypes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typesSpinner.setAdapter(dataAdapterTypes);

        piecesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                updateFields(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

    }

    public void updateFields(int position) {
        Piece p = this.pieces.get(position);
        nom_salle_ET.setText(p.getFonction());
        int type_index = this.types_salle.indexOf(p.getType());
        typesSpinner.setSelection(type_index);
    }

    public void onClickRetour(View view) {
        setResult(ACTIVITY_RESULT_VALUE);
        finish();
    }

    public void onClickValider(View view) throws IOException, JSONException {
        Piece p =  (Piece) piecesSpinner.getSelectedItem();
        String nom_salle = nom_salle_ET.getText().toString();
        String type_salle = (String) typesSpinner.getSelectedItem();
        Piece edited = new Piece();
        edited.setFonction(nom_salle);
        edited.setType(type_salle);

        //Creation url requete
        String url = getResources().getString(R.string.api_url)
                + "/pieces/" + p.getIdpiece();
        URL url_piece = new URL(url);

        // Conversion personne en Json
        ObjectMapper om = new ObjectMapper();
        om.registerModule(new JtsModule());
        JSONObject pieceJson = new JSONObject(om.writeValueAsString(edited));

        //Envoi de la requete PUT
        new EditPiece(url_piece, pieceJson).execute();
    }

    protected void onStop() {
        setResult(ACTIVITY_RESULT_VALUE);
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        setResult(ACTIVITY_RESULT_VALUE);
        super.onDestroy();
    }

    private class EditPiece extends AsyncTask<Void, Void, JSONObject> {
        private URL url;
        private JSONObject jsonObject;

        public EditPiece(URL url, JSONObject jsonObject) {
            this.url = url;
            this.jsonObject = jsonObject;
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) this.url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "*/*");
                connection.setRequestMethod("PUT");

                DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                out.writeBytes(this.jsonObject.toString());
                out.flush();
                out.close();

                int response = connection.getResponseCode();
                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder builder = new StringBuilder();

                    try (BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream()))) {
                        String line;

                        while ((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                    }
                    catch (IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(EditPieceActivity.this, R.string.read_error, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    return new JSONObject(builder.toString());
                }
                else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(EditPieceActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(EditPieceActivity.this, R.string.connect_error, Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            }

            finally {
                connection.disconnect(); // fermeture de la connexion
            }

            return null;
        }

        // Traitement de la réponse
        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            System.out.println(jsonObject);
            ObjectMapper om = new ObjectMapper();
            om.registerModule(new JtsModule());
            try {
                Piece p = om.readValue(jsonObject.toString(), Piece.class);
                System.out.println(p);
                finish();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}