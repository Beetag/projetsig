package fr.orleans.projetsig.modele;

public class Voisin {
    private long idVoisin;

    private long idPoint_source;

    private long idPoint_dest;

    public long getIdVoisin() {
        return idVoisin;
    }

    public void setIdVoisin(long idVoisin) {
        this.idVoisin = idVoisin;
    }

    public long getIdPoint_source() {
        return idPoint_source;
    }

    public void setIdPoint_source(long idPoint_source) {
        this.idPoint_source = idPoint_source;
    }

    public long getIdPoint_dest() {
        return idPoint_dest;
    }

    public void setIdPoint_dest(long idPoint_dest) {
        this.idPoint_dest = idPoint_dest;
    }
}
