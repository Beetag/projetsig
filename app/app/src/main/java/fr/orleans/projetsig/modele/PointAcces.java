package fr.orleans.projetsig.modele;

import androidx.annotation.NonNull;

import org.locationtech.jts.geom.Geometry;

import java.util.Objects;

public class PointAcces {
    private long idpoint;

    private String fonction;

    private int etage;

    private Geometry geom;

    public long getIdpoint() {
        return idpoint;
    }

    public void setIdpoint(long idpoint) {
        this.idpoint = idpoint;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public int getEtage() {
        return etage;
    }

    public void setEtage(int etage) {
        this.etage = etage;
    }

    public Geometry getGeom() {
        return geom;
    }

    public void setGeom(Geometry geom) {
        this.geom = geom;
    }

    @NonNull
    @Override
    public String toString() {
        if (this.fonction.length() > 25) {
            return this.fonction.substring(0,25) + "...";
        } else {
            return this.fonction;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PointAcces that = (PointAcces) o;
        return idpoint == that.idpoint;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idpoint);
    }
}
