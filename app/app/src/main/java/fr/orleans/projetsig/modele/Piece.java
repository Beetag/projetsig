package fr.orleans.projetsig.modele;

import androidx.annotation.NonNull;

import org.locationtech.jts.geom.Geometry;

import java.io.Serializable;


public class Piece implements Serializable {

    private long idpiece;

    private String fonction;

    private int etage;

    private String type;

    private Geometry geom;

    public long getIdpiece() {
        return idpiece;
    }

    public void setIdpiece(long idpiece) {
        this.idpiece = idpiece;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public int getEtage() {
        return etage;
    }

    public void setEtage(int etage) {
        this.etage = etage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Geometry getGeom() {
        return geom;
    }

    public void setGeom(Geometry geom) {
        this.geom = geom;
    }

    @NonNull
    @Override
    public String toString() {
        if (this.fonction.length() > 40) {
            return this.fonction.substring(0,40) + "...";
        } else {
            return this.fonction;
        }
    }
}
