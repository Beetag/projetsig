package fr.orleans.projetsig.dto;

public class CheminDTO {
    private long idArrivee;

    public long getIdArrivee() {
        return idArrivee;
    }

    public void setIdArrivee(long idArrivee) {
        this.idArrivee = idArrivee;
    }
}
