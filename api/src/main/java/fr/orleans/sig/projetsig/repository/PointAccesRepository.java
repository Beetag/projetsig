package fr.orleans.sig.projetsig.repository;

import fr.orleans.sig.projetsig.model.PointAcces;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PointAccesRepository extends CrudRepository<PointAcces, Long> {

    @Override
    List<PointAcces> findAll();
}
