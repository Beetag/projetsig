package fr.orleans.sig.projetsig.model;

import javax.persistence.*;

@Entity
@Table(name = "voisin")
public class Voisin {

    @Id
    @Column(name = "idvoisin")
    private long idVoisin;

    @Column(name = "idpointsource")
    private long idPointSource;

    @Column(name = "idpointdest")
    private long idPointDest;

    public long getIdVoisin() {
        return idVoisin;
    }

    public void setIdVoisin(long idVoisin) {
        this.idVoisin = idVoisin;
    }

    public long getIdPointSource() {
        return idPointSource;
    }

    public void setIdPointSource(long idPointSource) {
        this.idPointSource = idPointSource;
    }

    public long getIdPointDest() {
        return idPointDest;
    }

    public void setIdPointDest(long idPointDest) {
        this.idPointDest = idPointDest;
    }
}
