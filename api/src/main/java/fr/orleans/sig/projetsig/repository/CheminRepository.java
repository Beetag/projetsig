package fr.orleans.sig.projetsig.repository;

import fr.orleans.sig.projetsig.model.Chemin;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CheminRepository extends CrudRepository<Chemin, Integer> {
    @Override
    List<Chemin> findAll();

}
