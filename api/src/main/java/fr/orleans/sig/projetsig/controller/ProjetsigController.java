package fr.orleans.sig.projetsig.controller;


import fr.orleans.sig.projetsig.DTO.CheminDTO;
import fr.orleans.sig.projetsig.DTO.PersonneDTO;
import fr.orleans.sig.projetsig.DTO.PieceDTO;
import fr.orleans.sig.projetsig.model.*;
import fr.orleans.sig.projetsig.repository.*;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
public class ProjetsigController {

    @Autowired
    private PieceRepository pieceRepository;
    @Autowired
    private VoisinRepository voisinRepository;
    @Autowired
    private PointAccesRepository pointAccesRepository;
    @Autowired
    private PersonneRepository personneRepository;
    @Autowired
    private CheminRepository cheminRepository;

    /**
     * Récupère la liste des pièces
     * @return
     */
    @GetMapping(value="/pieces")
    public ResponseEntity<List<Piece>> getPieces() {
        List<Piece> pieces = pieceRepository.findAll();
        return ResponseEntity.ok().body(pieces);
    }

    /**
     * Récupère une pièce par son id
     * @param id l'identifiant de la pièce
     * @return un element JSON qui contient la structure d'un objet pièce
     */
    @GetMapping(value="/pieces/{id}")
    public ResponseEntity<Piece> getPieceById(@PathVariable long id) {
        Piece piece = pieceRepository.findById(id).orElse(null);
        return ResponseEntity.ok().body(piece);
    }

    /**
     * Change la fonction d'une pièce
     * @param id l'identifiant de la pièce
     * @param pieceDTO le DTO d'une pièce
     * @return un élément JSON qui contient la structure d'un objet pièce
     */
    @PutMapping(value = "/pieces/{id}")
    public ResponseEntity<Piece> setFonctionPiece(@PathVariable long id, @RequestBody PieceDTO pieceDTO) {
        Piece piece = pieceRepository.findById(id).orElse(null);
        if (piece == null) {
            return ResponseEntity.badRequest().build();
        }
        piece.setFonction(pieceDTO.getFonction());
        piece.setType(pieceDTO.getType());
        pieceRepository.save(piece);
        return ResponseEntity.ok().body(piece);
    }

    /**
     * Récupère tous les voisins
     * @return un élément JSON de la liste des voisins
     */
    @GetMapping(value = "/voisins")
    public ResponseEntity<List<Voisin>> getVoisins() {
        List<Voisin> voisins = voisinRepository.findAll();
        return ResponseEntity.ok().body(voisins);
    }

    /**
     * Récupère un voisin par son id
     * @param id l'identifiant d'un voisin
     * @return un élément JSON avec la structure d'un voisin
     */
    @GetMapping(value = "/voisins/{id}")
    public ResponseEntity<Voisin> getVoisinById(@PathVariable long id) {
        Voisin voisin = voisinRepository.findById(id).orElse(null);
        return ResponseEntity.ok().body(voisin);
    }

    /**
     * Récupère la liste des points d'accès
     * @return un élément JSON avec la liste des points d'accès
     */
    @GetMapping(value = "/point_acces")
    public ResponseEntity<List<PointAcces>> getPointAcces() {
        List<PointAcces> pointsAcces = pointAccesRepository.findAll();
        return ResponseEntity.ok().body(pointsAcces);
    }

    /**
     * Récupère un point d'accès par son id
     * @param id l'identifiant du point d'accès
     * @return un élément JSON avec la structure d'un point d'accès
     */
    @GetMapping(value = "/point_acces/{id}")
    public ResponseEntity<PointAcces> getPointAcces(@PathVariable long id) {
        PointAcces pointAcces = pointAccesRepository.findById(id).orElse(null);
        return ResponseEntity.ok().body(pointAcces);
    }

    /**
     * Récupère la liste des personnes
     * @return un élément JSON avec la structure d'une personne
     */
    @GetMapping(value = "/personnes")
    public ResponseEntity<List<Personne>> getPersonnes() {
        List<Personne> personnes = personneRepository.findAll();
        return ResponseEntity.ok().body(personnes);
    }

    /**
     * Récupère la position d'une personne via son id
     * @param id l'identifiant d'une personne
     * @return un élément JSON avec la structure d'une personne
     */
    @GetMapping(value = "/personnes/{id}")
    public ResponseEntity<Personne> getPositionPersonne(@PathVariable long id) {
        Personne personne = personneRepository.findById(id).orElse(null);
        return ResponseEntity.ok().body(personne);
    }

    /**
     * Change la position d'une personne
     * @param id l'identifiant d'une personne
     * @param personneDTO le DTO de personne
     * @return un élément JSON avec la structure d'une personne
     */
    @PutMapping(value = "/personnes/{id}")
    public ResponseEntity<Personne> setPositionPersonne(@PathVariable long id, @RequestBody PersonneDTO personneDTO) {
        Personne personne = personneRepository.findById(id).orElse(null);
        if (personne == null) {
            return ResponseEntity.badRequest().build();
        }
        personne.setEtage(personneDTO.getEtage());
        personne.setPosition(personneDTO.getPosition());
        personneRepository.save(personne);
        return ResponseEntity.ok().body(personne);
    }

    /**
     * Récupère la liste des chemins
     * @return un élément JSON avec la structure d'un chemin
     */
    @GetMapping(value = "/chemins")
    public ResponseEntity<List<Chemin>> getChemins() {
        List<Chemin> chemins = cheminRepository.findAll();
        return ResponseEntity.ok().body(chemins);
    }

    /**
     * Supprime un chemin
     * @return une réponse HTTP
     */
    @DeleteMapping(value = "/chemins")
    public ResponseEntity<String> supprimerChemin() {
        cheminRepository.deleteAll();
        return ResponseEntity.ok().build();
    }

    /**
     * Trouve le chemin le plus court vers la destination choisie
     * @param cheminDTO le DTO de chemin
     * @return un élément JSON avec la liste des points a emprunter pour le chemin
     */
    @PostMapping(value = "/trouverchemin")
    public ResponseEntity<List<PointAcces>> trouverChemin(@RequestBody CheminDTO cheminDTO){
        Personne personne = personneRepository.findById((long) 1).orElse(null);
        if (personne == null) {
            return ResponseEntity.badRequest().build();
        }

        long idPointCourant = cheminDTO.getIdArrivee();

        List<PointAcces> queue = pointAccesRepository.findAll();

        long idPosPersonne = getIdPosPersonne(personne, queue);

        List<Integer> dist = new ArrayList<>();
        List<PointAcces> predecesseur = new ArrayList<>();
        List<PointAcces> chemin = new ArrayList<>();

        // initialisation de toutes les distances à la valeur max
        for(int i = 0; i <= queue.size(); i++){
            dist.add(Integer.MAX_VALUE);
            // valeur temporaire
            predecesseur.add(null);
        }
        // Set départ à 0
        dist.set((int) idPosPersonne - 1, 0);

        // Algo de Dijkstra
        while(queue.size() > 0){
            // On cherche le point le plus proche
            PointAcces p1 = trouverMin(queue, dist);
            queue.remove(p1);
            // On récupère ses voisins
            List<Voisin> listeVoisins = voisinRepository.findAllByIdPointSource(p1.getIdpoint());
            // Parcours des voisins
            for (int i = 0; i < listeVoisins.size(); i++){
                // Si il est plus plus proche on met à jour la distance
                if( dist.get((int) listeVoisins.get(i).getIdPointDest() - 1) > dist.get((int) p1.getIdpoint() - 1) + 1){
                    dist.set((int) listeVoisins.get(i).getIdPointDest() - 1, dist.get((int) p1.getIdpoint() - 1) + 1);
                    // On l'ajoute à la liste des prédecesseurs
                    predecesseur.set((int) listeVoisins.get(i).getIdPointDest() - 1, p1);
                }
            }
        }
        // On cherche le chemin existant
        while (idPointCourant != idPosPersonne){
            chemin.add(pointAccesRepository.findById(idPointCourant).orElse(null));
            idPointCourant = predecesseur.get((int) idPointCourant - 1).getIdpoint();
        }
        // Ajout du point de départ
        chemin.add(pointAccesRepository.findById(idPosPersonne).orElse(null));
        Collections.reverse(chemin);

        //Suppression de l'ancien chemin stocké en bd
        cheminRepository.deleteAll();

        GeometryFactory geometryFactory = new GeometryFactory();

        // Parcours de la liste de points
        for (int i = 0; i+1 < chemin.size(); i++) {
            // Si deux points se suivant sont au même étage
            if (chemin.get(i).getEtage() == chemin.get(i+1).getEtage()) {
                Coordinate co_deb = new CoordinateXY(
                        chemin.get(i).getGeom().getCoordinate().x, chemin.get(i).getGeom().getCoordinate().y);
                Coordinate co_fin = new CoordinateXY(
                        chemin.get(i+1).getGeom().getCoordinate().x, chemin.get(i+1).getGeom().getCoordinate().y);
                CoordinateSequence cs = new CoordinateArraySequence(new Coordinate[]{co_deb, co_fin});
                // On créé une nouvelle ligne reliant ces deux points
                LineString ls = new LineString(cs, geometryFactory);
                Chemin ch = new Chemin();
                ch.setEtage(chemin.get(i).getEtage());
                ch.setLigne(ls);
                // On sauvegarde le chemin créé
                cheminRepository.save(ch);
            }
        }

        return ResponseEntity.ok().body(chemin);

    }

    /**
     * Permet de trouver le point d'accès le plus proche
     * @param queue les points d'accès que l'on teste
     * @param dist la distance de chaque point par rapport à la position initiale
     * @return le point d'accès le plus proche
     */
    private PointAcces trouverMin(List<PointAcces> queue, List<Integer> dist){
        int mini = Integer.MAX_VALUE;
        PointAcces resultat = null;

        for (int i = 0; i < queue.size(); i++){
            int idPointCourrant = (int) queue.get(i).getIdpoint();
            if(dist.get(idPointCourrant - 1) < mini){
                mini = dist.get(idPointCourrant - 1);
                resultat = queue.get(i);
            }
        }

        return resultat;
    }

    /**
     * Récupère l'identifiant du point correspondant à la position d'une personne
     * @param p la personne dont on cherche la position
     * @param points la liste des point d'accès
     * @return l'identifiant du point de la position actuelle de la personne
     */
    private long getIdPosPersonne(Personne p, List<PointAcces> points) {
        Coordinate cPers = p.getPosition().getCoordinate();
        for (PointAcces point : points) {
            if (p.getEtage() == point.getEtage() && point.getGeom().getCoordinate().compareTo(cPers) == 0) {
                return point.getIdpoint();
            }
        }

        return -1;
    }

}
