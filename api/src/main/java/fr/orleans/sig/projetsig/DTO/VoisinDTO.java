package fr.orleans.sig.projetsig.DTO;

public class VoisinDTO {

    private long idPointSource;
    private long idPointDest;

    public long getIdPointSource() {
        return idPointSource;
    }

    public void setIdPointSource(long idPointSource) {
        this.idPointSource = idPointSource;
    }

    public long getIdPointDest() {
        return idPointDest;
    }

    public void setIdPointDest(long idPointDestination) {
        this.idPointDest = idPointDestination;
    }
}
