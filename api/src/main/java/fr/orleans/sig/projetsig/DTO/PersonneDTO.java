package fr.orleans.sig.projetsig.DTO;

import org.locationtech.jts.geom.Geometry;

public class PersonneDTO {

    private int etage;
    private Geometry position;

    public int getEtage() {
        return etage;
    }

    public void setEtage(int etage) {
        this.etage = etage;
    }

    public Geometry getPosition() {
        return position;
    }

    public void setPosition(Geometry position) {
        this.position = position;
    }
}
