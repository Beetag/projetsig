package fr.orleans.sig.projetsig.repository;

import fr.orleans.sig.projetsig.model.Voisin;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VoisinRepository extends CrudRepository<Voisin, Long> {

    @Override
    List<Voisin> findAll();

    List<Voisin> findAllByIdPointSource(long idPointSource);

}
