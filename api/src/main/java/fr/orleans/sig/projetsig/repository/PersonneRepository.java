package fr.orleans.sig.projetsig.repository;

import fr.orleans.sig.projetsig.model.Personne;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonneRepository extends CrudRepository<Personne, Long> {

    @Override
    List<Personne> findAll();
}
