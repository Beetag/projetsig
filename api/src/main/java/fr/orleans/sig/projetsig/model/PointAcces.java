package fr.orleans.sig.projetsig.model;

import org.locationtech.jts.geom.Geometry;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "point_acces")
public class PointAcces {

    @Id
    private long idpoint;

    private String fonction;

    private int etage;

    private Geometry geom;

    public long getIdpoint() {
        return idpoint;
    }

    public void setIdpoint(long idpoint) {
        this.idpoint = idpoint;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public int getEtage() {
        return etage;
    }

    public void setEtage(int etage) {
        this.etage = etage;
    }

    public Geometry getGeom() {
        return geom;
    }

    public void setGeom(Geometry geom) {
        this.geom = geom;
    }
}
