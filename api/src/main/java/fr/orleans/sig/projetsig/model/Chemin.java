package fr.orleans.sig.projetsig.model;

import org.locationtech.jts.geom.Geometry;

import javax.persistence.*;

@Entity
@Table(name="chemin")
public class Chemin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idchemin;

    private int etage;

    private Geometry ligne;

    public long getIdchemin() {
        return idchemin;
    }

    public void setIdchemin(long idchemin) {
        this.idchemin = idchemin;
    }

    public int getEtage() {
        return etage;
    }

    public void setEtage(int etage) {
        this.etage = etage;
    }

    public Geometry getLigne() {
        return ligne;
    }

    public void setLigne(Geometry ligne) {
        this.ligne = ligne;
    }
}
