package fr.orleans.sig.projetsig.DTO;

import org.locationtech.jts.geom.Geometry;

public class PointAccesDTO {

    private String fonction;
    private int etage;
    private Geometry geometry;


    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public int getEtage() {
        return etage;
    }

    public void setEtage(int etage) {
        this.etage = etage;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }
}
