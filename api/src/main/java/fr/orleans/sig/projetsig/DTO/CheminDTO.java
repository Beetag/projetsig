package fr.orleans.sig.projetsig.DTO;

public class CheminDTO {

    private long idArrivee;

    public long getIdArrivee() {
        return idArrivee;
    }

    public void setIdArrivee(long idArrivee) {
        this.idArrivee = idArrivee;
    }
}
