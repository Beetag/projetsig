package fr.orleans.sig.projetsig.model;

import org.locationtech.jts.geom.Geometry;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="personne")
public class Personne {

    @Id
    private long idpersonne;

    private int etage;

    private Geometry position;

    public long getIdpersonne() {
        return idpersonne;
    }

    public void setIdpersonne(long idpersonne) {
        this.idpersonne = idpersonne;
    }

    public int getEtage() {
        return etage;
    }

    public void setEtage(int etage) {
        this.etage = etage;
    }

    public Geometry getPosition() {
        return position;
    }

    public void setPosition(Geometry position) {
        this.position = position;
    }
}
