package fr.orleans.sig.projetsig.model;


import org.locationtech.jts.geom.Geometry;

import javax.persistence.*;

@Entity
@Table(name="piece")
public class Piece {

    @Id
    private long idpiece;

    private String fonction;

    private int etage;

    private String type;

    private Geometry geom;

    public long getIdpiece() {
        return idpiece;
    }

    public void setIdpiece(long idpiece) {
        this.idpiece = idpiece;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public int getEtage() {
        return etage;
    }

    public void setEtage(int etage) {
        this.etage = etage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Geometry getGeom() {
        return geom;
    }

    public void setGeom(Geometry geom) {
        this.geom = geom;
    }
}
