package fr.orleans.sig.projetsig.repository;

import fr.orleans.sig.projetsig.model.Piece;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PieceRepository extends CrudRepository<Piece, Long> {

    @Override
    List<Piece> findAll();


}
