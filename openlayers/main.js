import 'ol/ol.css';
import ImageWMS from 'ol/source/ImageWMS';
import Map from 'ol/Map';
import Projection from 'ol/proj/Projection';
import TileWMS from 'ol/source/TileWMS';
import View from 'ol/View';
import {Image as ImageLayer, Tile as TileLayer} from 'ol/layer';

var url = new URL(window.location.href);
var etage = url.searchParams.get("etage");
console.log(etage);

var layers = [
  new ImageLayer({
    source: new ImageWMS({
      crossOrigin: 'anonymous',
      params: {
        'LAYERS': 'sig:etage'+etage,
        'VERSION': '1.1.0'
      },
      url: 'http://192.168.1.19:8080/geoserver/sig/wms',
    }),
  })];

// A minimal projection object is configured with only the SRS code and the map
// units. No client-side coordinate transforms are possible with such a
// projection object. Requesting tiles only needs the code together with a
// tile grid of Cartesian coordinates; it does not matter how those
// coordinates relate to latitude or longitude.
var projection = new Projection({
  code: 'EPSG:404000',
  units: 'degrees',
});

var map = new Map({
  layers: layers,
  target: 'map',
  view: new View({
    center: [1.25, -3],
    projection: projection,
    zoom: 7,
  }),
});
